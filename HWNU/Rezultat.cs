﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWNU
{
    public class Rezultat
    {
        public string CalcValidator(string calc = null)
        {
            if (calc == null)
            {
                Console.WriteLine("введите выражение: ");
                calc = Console.ReadLine();
            }
            bool isCorrected = false;
            while (true)
            {
                for (int i = 0; i < calc.Length; i++)
                {
                    if (calc[i] == '0' || calc[i] == '1' || calc[i] == '2' || calc[i] == '3' || calc[i] == '4' || calc[i] == '5' || calc[i] == '6'
                        || calc[i] == '7' || calc[i] == '8' || calc[i] == '9' || calc[i] == '(' || calc[i] == ')' || calc[i] == '*'
                        || calc[i] == '/' || calc[i] == '+' || calc[i] == '-' || calc[i] == '.')
                    {
                        if (calc.Length < 2 || calc[0] == '*' || calc[0] == '/')
                        {
                            isCorrected = false;
                            break;
                        }
                        if (calc[i] == '+' && calc[i + 1] == '+' || calc[i] == '-' && calc[i + 1] == '-'
                            || calc[i] == '*' && calc[i + 1] == '*' || calc[i] == '/' && calc[i + 1] == '/' || calc[i] == '(' && calc[i + 1] == ')')
                        {
                            isCorrected = false;
                            break;
                        }
                        isCorrected = true;
                    }
                    else
                    {
                        isCorrected = false;
                        break;
                    }
                }
                if (isCorrected == false)
                {
                    Console.WriteLine("введите корректное число. повторите еще раз");
                    return "введите корректное число. повторите еще раз";
                }
                else
                {
                    if (calc[0] == '-')
                    {
                        calc = calc.Insert(0, "0");
                    }
                    else if (calc[0] == '+')
                    {
                        calc = calc.Remove(0, 1);
                    }
                    return calc;
                }
            }
        }

        public string PereborStroki(string vvod)
        {
            var korzina = new Stack<char>();
            string a = null;
            int b;
            for (int i = 0; i < vvod.Length; i++)
            {
                if (Char.IsDigit(vvod[i]) || vvod[i] == '.')
                {
                    if (i > 0 && (!Char.IsDigit(vvod[i - 1]) && vvod[i - 1] != '.'))
                        a += " ";
                    a += Convert.ToString(vvod[i]);
                }
                else
                {
                    b = Deystviya(vvod[i]);
                    if (korzina.Count == 0)
                    {
                        korzina.Push(vvod[i]);
                    }
                    else
                    {
                        if (b != Deystviya(korzina.Peek()))
                        {
                            if (b > Deystviya(korzina.Peek()))
                            {
                                korzina.Push(vvod[i]);
                            }
                            else
                   if (b < Deystviya(korzina.Peek()))
                            {
                                if (b == 2)
                                {
                                    if (korzina.Peek() == '(')
                                    {
                                        korzina.Pop();
                                    }
                                    else
                                    if (korzina.Peek() != '(')
                                    {
                                        while (korzina.Peek() != '(')
                                        {
                                            a += korzina.Pop();
                                        }
                                        korzina.Pop();
                                    }
                                }
                                else
                                {
                                    if (b == 1)
                                    {
                                        korzina.Push(vvod[i]);
                                    }
                                    else
                                    {
                                        a += korzina.Pop();
                                        korzina.Push(vvod[i]);
                                    }
                                }
                            }
                        }
                        else
                            if (b == Deystviya(korzina.Peek()))
                        {
                            a += korzina.Pop();
                            korzina.Push(vvod[i]);
                        }
                    }
                }
            }
            while (korzina.Count > 0)
            {
                a += korzina.Pop();
            }
            return a;
        }

        public double Vycislenieya(string vvod2)
        {
            var korzina2 = new Stack<double>();
            double c = 0;
            double d = 0;
            string cislo = "";
            for (int i = 0; i < vvod2.Length; i++)
            {
                if (Char.IsDigit(vvod2[i]) || vvod2[i] == '.')
                {
                    cislo += vvod2[i];
                    if (i + 1 == vvod2.Length || !Char.IsDigit(vvod2[i + 1]) && vvod2[i + 1] != '.')
                    {
                        korzina2.Push(double.Parse(cislo));
                        cislo = "";
                    }
                }
                else if (Deystviya(vvod2[i]) > 0)
                {
                    c = korzina2.Pop();
                    d = korzina2.Pop();
                    switch (vvod2[i])
                    {
                        case '*':
                            c = d * c;
                            korzina2.Push(c); break;
                        case '/':
                            if (c == 0)
                            {
                                c = 0;
                            }
                            else
                            {
                                c = d / c;
                            }
                            korzina2.Push(c); break;
                        case '+':
                            c = d + c;
                            korzina2.Push(c); break;
                        case '-':
                            c = d - c;
                            korzina2.Push(c); break;
                    }
                }
            }
            return c;
        }

        public int Deystviya(int vvod3)
        {
            switch (vvod3)
            {
                case '*': return 5;
                case '/': return 5;
                case '+': return 4;
                case '-': return 4;
                case ')': return 2;
                case '(': return 1;
                default: return 0;
            }
        }
    }
}

using HWNU;
using NUnit.Framework;

namespace HWNU_test
{
    public class Tests
    {
        [TestCase("++", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("--", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("**", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("//", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("()", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("*", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("/", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("a", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("1", "������� ���������� �����. ��������� ��� ���")]
        [TestCase(" ", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("+", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("-", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("a+1", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("1++1", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("1--1", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("1**1", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("1//1", "������� ���������� �����. ��������� ��� ���")]
        [TestCase("-1", "0-1")]
        [TestCase("+1", "1")]
        [TestCase("11", "11")]
        [TestCase("1+1", "1+1")]
        [TestCase("1-1", "1-1")]
        [TestCase("1*1", "1*1")]
        [TestCase("1/1", "1/1")]
        [TestCase("(1+1)*2", "(1+1)*2")]
        [TestCase("(1+1)/2", "(1+1)/2")]
        [TestCase("(1+1)+2", "(1+1)+2")]
        [TestCase("(1+1)-2", "(1+1)-2")]

        public void CheckValidator(string word, string exp)
        {
            Rezultat calcvalidator = new Rezultat();
            string act = calcvalidator.CalcValidator(word);
            Assert.AreEqual(exp, act);
        }

        [TestCase("*", 5)]
        [TestCase("/", 5)]
        [TestCase("+", 4)]
        [TestCase("-", 4)]
        [TestCase(")", 2)]
        [TestCase("(", 1)]
        [TestCase("!", 0)]
        public void CheckPriority(char word, int exp)
        {
            Rezultat priority = new Rezultat();
            int act = priority.Deystviya(word);
            Assert.AreEqual(exp, act);
        }

        [TestCase("1 1+", "2")]
        [TestCase("1 1-", "0")]
        [TestCase(" 1 1+ 2*", "4")]
        [TestCase(" 1 1+ 2/", "1")]
        [TestCase(" 1 1+ 2+", "4")]
        [TestCase(" 1 1+ 2-", "0")]
        [TestCase("1 3*", "3")]
        [TestCase("4 2/", "2")]
        [TestCase(" 1.1 1+ 2-", "0.10000000000000009")]
        [TestCase(" 1.1 1+ 2+", "4.1")]
        [TestCase(" 1.1 1+ 2*", "4.2")]
        [TestCase(" 1.1 1+ 2/", "1.05")]
        public void CheckVichslenya(string cislo, double exp)
        {
            Rezultat vicislenya = new Rezultat();
            double act = vicislenya.Vycislenieya(cislo);
            Assert.AreEqual(exp, act);
        }

        [TestCase("4/2", "4 2/")]
        [TestCase("1+1", "1 1+")]
        [TestCase("1-1", "1 1-")]
        [TestCase("4*2", "4 2*")]
        [TestCase("2*(1+1)", "2 1 1+*")]
        [TestCase("2/(1+1)", "2 1 1+/")]
        [TestCase("2-(1+1)", "2 1 1+-")]
        [TestCase("2+(1+1)", "2 1 1++")]

        public void CheckVPereborStroki(string cislo, string exp)
        {
            Rezultat pereborstroki = new Rezultat();
            string act = pereborstroki.PereborStroki(cislo);
            Assert.AreEqual(exp, act);
        }
    }
}